from django.contrib import admin

from .models import Question, Human, Tag, Theme, Video


class VideoInline(admin.StackedInline):
    model = Question
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    inlines = [VideoInline, ]


admin.site.register(Question)
admin.site.register(Human)
admin.site.register(Tag)
admin.site.register(Theme)
admin.site.register(Video,QuestionAdmin)

# Register your models here.

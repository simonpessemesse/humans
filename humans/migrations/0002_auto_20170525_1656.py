# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-25 16:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('humans', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='human',
            name='introVideo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='intro', to='humans.Video'),
        ),
        migrations.AlterField(
            model_name='human',
            name='waitingVideos',
            field=models.ManyToManyField(null=True, related_name='waiting', to='humans.Video'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='questions',
            field=models.ManyToManyField(null=True, to='humans.Question'),
        ),
    ]

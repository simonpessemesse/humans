# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-21 11:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('humans', '0007_auto_20170621_1033'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='videoL',
        ),
        migrations.RemoveField(
            model_name='video',
            name='videoM',
        ),
    ]

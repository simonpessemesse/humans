rm *.low.mp4 *.medium.mp4
for i in $(ls -1 *mp4|grep -v medium.mp4$|grep -v low.mp4$)
do
    echo "$i"
    if [ ! -f "$i".medium.mp4 ]; then
        ffmpeg -i "$i" -acodec copy -vcodec libx264 -filter:v scale=640:-1 "$i".medium.mp4
    fi
    if [ ! -f "$i".low.mp4 ]; then
        ffmpeg -i "$i" -acodec copy -vcodec libx264 -filter:v scale=480:238 "$i".low.mp4
    fi
done;

from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import Question,Human,Tag,Theme,Video
from collections import OrderedDict

# Create your views here.
def index(request):
    humans=Human.objects.all()
    return render(request,'humans/index.html',{'humans':humans})



def future(request, human):
    human=Human.objects.get(name=human)
    videos=Video.objects.filter(human=human)
    questions=Question.objects.filter(video__human=human)
    questions=list(questions)
    questions.sort(key=lambda t:t.order or 0)
    qParTheme={}
    qParTag={}

    for q in questions:
        for tag in q.tags.all():
            if tag in qParTag:
                qParTag[tag].append(q)
            else:
                qParTag[tag]=[q]
        if q.theme in qParTheme:
            qParTheme[q.theme].append(q)
        else:
            qParTheme[q.theme]=[q]

    ord=OrderedDict(sorted(qParTheme.items(), key=lambda t: t[0].order or 0))
    for v in ord.values():
        v.sort(key=lambda t: t.order or 0)
    qParTheme=ord

    ord=OrderedDict(sorted(qParTag.items(), key=lambda t: t[0].order or 0))
    for v in ord.values():
        v.sort(key=lambda t: t.order or 0)
    qParTag=ord

    return render(request,'humans/2048.html',{'human':human,'videos':videos,'questions':questions,'themes':qParTheme,'tags':qParTag})

def human(request, human):
    human=Human.objects.get(name=human)
    videos=Video.objects.filter(human=human)
    questions=Question.objects.filter(video__human=human)
    qParTheme={}
    qParTag={}

    for q in questions:
        for tag in q.tags.all():
            if tag in qParTag:
                qParTag[tag].append(q)
            else:
                qParTag[tag]=[q]
        if q.theme in qParTheme:
            qParTheme[q.theme].append(q)
        else:
            qParTheme[q.theme]=[q]

    ord=OrderedDict(sorted(qParTheme.items(), key=lambda t: t[0].order or 0))
    for v in ord.values():
        v.sort(key=lambda t: t.order or 0)
    qParTheme=ord

    ord=OrderedDict(sorted(qParTag.items(), key=lambda t: t[0].order or 0))
    for v in ord.values():
        v.sort(key=lambda t: t.order or 0)
    qParTag=ord

    return render(request,'humans/human.html',{'human':human,'videos':videos,'questions':questions,'themes':qParTheme,'tags':qParTag})


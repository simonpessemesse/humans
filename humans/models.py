from django.db import models

# Create your models here.


class Video(models.Model):
    human=models.ForeignKey('Human',default="1")
    name=models.CharField(max_length=200,null=True,blank=True)
    video=models.FileField(upload_to="videos/")
  #  videoM=models.FileField(upload_to="videos/",null=True,blank=True)
   # videoL=models.FileField(upload_to="videos/",null=True,blank=True)
    def __str__(self):
        if self.name:
            n=self.name
        else:
            n=self.video
        return str(self.human)+":"+str(n)
class Theme(models.Model):
    order=models.FloatField(blank=True,null=True)
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

class Question(models.Model):
    name = models.CharField(max_length=200)
    order=models.FloatField(blank=True,null=True)
    video = models.ForeignKey(Video)
    theme=models.ForeignKey(Theme)
    tags=models.ManyToManyField('Tag',blank=True,null=True)
    def __str__(self):
        return self.name

class Tag(models.Model):
    order=models.FloatField(blank=True,null=True)
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

class Human(models.Model):
    name = models.CharField(max_length=200)
    mot_de_passe=models.CharField(max_length=200,blank=True,null=True)
    introVideo = models.ForeignKey(Video,related_name='intro',null=True,blank=True)
    longSilence = models.ForeignKey(Video,related_name='long',null=True,blank=True)
    courtSilence = models.ForeignKey(Video,related_name='court',null=True,blank=True)
    posezmoi1 = models.ForeignKey(Video,related_name='posez1',null=True,blank=True)
    posezmoi2 = models.ForeignKey(Video,related_name='posez2',null=True,blank=True)
    waitingVideos = models.ManyToManyField(Video,related_name='waiting',null=True,blank=True)
    def __str__(self):
        return self.name

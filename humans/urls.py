from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^index\.html$', views.index, name='index'),
    url(r'^(?P<human>[0-9a-zA-Z]+)/index.html$', views.human, name='human'),
    url(r'^(?P<human>[0-9a-zA-Z]+)/2048.html$', views.future, name='future'),
]
